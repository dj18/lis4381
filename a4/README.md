> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381-0001

## Darion Jattansingh

### A4 # Requirements:

*Sub-Heading:*

1. Create a web apllication
2. Use jQuery, PHP, Javascript, HTML, and CSS
3. Questions

#### README.md file should include the following items:

* Screenshot of Carousel;
* Screenshot of form validation;
* Screenshot of form not validated;

#### Assignment Screenshots:

*Screenshot of Carousel:

![Carousel Screenshot](img/screenshot1.png)

*Screenshot of form validation:

![Valid Form Screenshot](img/screenshot2.png)

*Screenshot of invalid form:

![Invalid Form Screenshot](img/screenshot3.png)
