> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381-0001

## Darion Jattansingh

### P1 # Requirements:

*Requirements:*

1. Create an interactive business card
2. Add contact
3. Questions

#### README.md file should include the following items:

* Screenshot of main activity running
* Screenshot of running application’s second user interface;

#### Assignment Screenshots:

*Screenshot of running application's first user interface:

![Business Card First Activity Screenshot](img/main.png)

*Screenshot of running application's second user interface:

![Business Card Second Activity Screenshot](img/second.png)

