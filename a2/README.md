> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Darion Jattansingh

### Assignment #2 Requirements:

1. Distributed Version Control with Bitbucket
2. Develop two activity screens
3. Questions


*Screenshots:*

*Screenshot of Android Studio running:

![Android Studio Main Activity Screenshot](img/mainactivity.png)

*Screenshot of running java Hello*:

![Android Studio Second Activity Screenshot](img/secondactivity.png)


