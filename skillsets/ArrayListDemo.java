import java.util.Scanner;
import java.util.*; //need for Arraylist

public class ArrayListDemo
{
    public static void main(String args[])
    {
        //display operational messages
        System.out.println("Program populates ArrayList of strings with user-entered animal type values.");
        System.out.println("Examples: Polar bear, Guniea pig, dog, cat bird.");
        System.out.println("Program continues to collect user-entered values until user types \"n\".");
        System.out.println("Program displays ArrayList values after each iteration, as well as size.");

        System.out.println(); //print blank line

        //create program variables/objects
        //create Scanner object
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>(); //create String type ArrayList
        String myStr = "";
        String choice = "y";
        int num = 0;

        while (choice.equals("y"))
        { 
            System.out.print("Enter animal type: ");
            myStr = sc.nextLine();
            obj.add(myStr); //add String object
            num = obj.size(); //returns ArrayList size
            System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
            System.out.print("\nContinue? Enter y or n: ");
            choice = sc.next();
            sc.nextLine();
        }
    }
}