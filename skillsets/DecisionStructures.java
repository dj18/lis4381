import java.util.Scanner;

public class DecisionStructures
{
    public static void main(String[] args) {
        
        //Display operational messages
        System.out.println ("Program evaluates user-entered characters.");
        System.out.println ("Use following characters: W or w, C or c, H or h, N or n.");
        System.out.println ("Use following decision structures: if...else, and switch.");
        System.out.println ();
     
        String myStr = "";
        char myChar = ' ';
        Scanner sc = new Scanner(System.in);

        System.out.println(); //print blank line

        /*
            Note: Currently, there is no API method to get a character from the scanner.
            Solution: get String using scanner.next() and invoke String.charAt(0) method on returned String.
        */

        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none)");
        System.out.print("Enter phone type: ");
        myStr = sc.next().toLowerCase();
        myChar = myStr.charAt(0);
        System.out.println(); //print blank line

        System.out.println ("\nif...else:");

        if (myChar == 'w')
            System.out.println ("Phone type: work");
        else if (myChar == 'c')
            System.out.println("Phone Type: cell");
        else if (myChar == 'h')
            System.out.println("Phone Type: home");
        else if (myChar == 'n')
            System.out.println("Phone Type: none");
        else    
            System.out.println("Incorrect character entry.");
       
        System.out.println(); //print blank line

        System.out.println("switch:");
        switch (myChar)
            {
            case 'w':
                 System.out.println("Phone Type: work");
                 break;
            case 'c':
                 System.out.println("Phone Type: cell");
                 break;
            case 'h':
                 System.out.println("Phone Type: home");
                 break;
            case 'n':
                 System.out.println("Phone Type: none");
                 break;
           default:
                 System.out.println("Phone Type: Incorrect character entry.");
                 break;
            
             }

    }//end of main
}//end of class