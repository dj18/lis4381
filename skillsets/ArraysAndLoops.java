import java.util.Scanner;

public class ArraysAndLoops
{
    public static void main(String[] args) {
        
        //Display operational messages
        System.out.println ("Program loops through array of strings.");
        System.out.println ("Use following values: dog, cat, bird, fish, insect.");
        System.out.println ("Use following loop structures: for, enhanced for, while. Posttest loop: do...while");
        System.out.println ();
        System.out.println ("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println ();
     
        //Declare animals string
         String[] animals = {"dog", "cat", "bird", "fish", "insect"}; 
   
            //for loop
            System.out.println("for loop:");
             for (int i = 0; i < animals.length; i++) {
             System.out.println(animals[i]);
            }//end of for loop

        System.out.println(); //print blank line

            //enahanced for loop
            System.out.println("enhanced for loop:");
            for(String list: animals){
            System.out.println(list);
            }//end of enhanced loop

        System.out.println(); //print blank line

            //while loop
            System.out.println("while loop:");
            int i=0;
            while (i<animals.length){
                System.out.println(animals[i]);
                i++;
            }//end of while loop 

        System.out.println(); //print blank line

            //do while loop
            i = 0; //reassign 0 to test variable
            System.out.println("do while loop:");
            do {
            System.out.println(animals[i]);
            i++;
            } while (i < animals.length);

    }//end of main
}//end of class