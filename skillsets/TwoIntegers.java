import java.util.Scanner;  //import only the Scanner class

public class TwoIntegers { 
  
  public static void main(String[] args) { 
    
        //Create Scanner object
        Scanner input = new Scanner (System.in);
       
        //declare variable
        int a, b, bigger;
        
        //prompt the user for numbers
        System.out.print("Please enter two numbers: ");

        //read the numbers
        a = input.nextInt();
        b = input.nextInt();
        
        //Display message
        
        if(a>b)
        {
          bigger = a;
        }
        else
        {
          bigger = b;
        }
        
        System.out.println("The largest of the two numbers is " +bigger);
       
  }//end of main
  
}//end classs