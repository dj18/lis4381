import java.util.Scanner;

class LargestOfThreeNumbers
{
    public static void main(String args[])
    {
        //display operational messages
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");
        System.out.println(); //print blank line

        //declare variables and create Scanner object
        int num1 = 0, num2 = 0, num3 = 0;
        Scanner input = new Scanner(System.in);

        System.out.print("Please enter first number: ");
        while (!input.hasNextInt())
        {
        System.out.println("Not valid integer!\n");
        input.next(); //Important! If omitted, will go into infinite loop on invalid input!
        System.out.print("Please try again. Enter first number: ");
        }
        num1 = input.nextInt(); 

        System.out.print("\nPlease enter second number: ");
        while (!input.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                input.next(); //Important! If omitted, will go into infinite loop on invalid input!
                System.out.print("Please try again. Enter second number: ");
            }
        num2 = input.nextInt();

        System.out.print("\nPlease enter third number: ");
        while (!input.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                input.next(); //Important! If omitted, will go into infinite loop on invalid input!
                System.out.print("Please try again. Enter third number: ");
            }
        num3 = input.nextInt();

        System.out.println(); //print blank line

        if ( num1 > num2 && num1 > num3 )
            System.out.println("First number is largest.");
        else if ( num2 > num1 && num2 > num3 )
            System.out.println("Second number is largest.");
        else if ( num3 > num1 && num3 > num2 )
            System.out.println("Third number is largest.");
        else 
            System.out.println("Integers are equal.");
            }
        }