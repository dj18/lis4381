import java.util.Scanner;  //import only the Scanner class

public class EvenOrOdd { 
  
  public static void main(String[] args) { 
    
        //Create Scanner object
        Scanner input = new Scanner (System.in);
       
        //declare variable
        int num;
        
        //prompt the user for their age
        System.out.print("Please enter a number: ");
        
        //read the users age
        num = input.nextInt();
    
        //Display message
        
        if(num % 2 == 0)
        {
            System.out.println("This number is even.");
        }
        else
        {
            System.out.println("This number is odd.");
        }

    
  }//end of main
  
}//end classs