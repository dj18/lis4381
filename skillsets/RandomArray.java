import java.util.Scanner;
import java.util.Random;

class RandomArray
{
    public static void main(String args[])
    {
        //display operational messages
        System.out.println("Program prompts user to enter desired number of pseudurandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhanced for, while, do...while.");
        System.out.println(); // print blank line

        //http://stackoverflow.com/questions/363681/generating-random-integers-in-a-specific-range
        //http://stackoverflow.com/questions/20380991/fill-an-array-with-random-numbers
        //http://stackoverflow.com/questions/20389890/generating-a-random-number-between-1-and-10-java
        //http://mindprod.com/jgloss/pseudorandom.html
        //https://www.geneseo.edu/~baldwin/reference/random.html

        Scanner sc = new Scanner(System.in);
        Random r = new Random(); //instantiate random object value
        int arraySize = 0;
        int i = 0;

        //prompt user for number of randomly generated numbers 
        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        arraySize = sc.nextInt();

        //Java style String[] myArray
        //C++ style String myArray[]
        int myArray[] = new int[arraySize];

        /*
        If a Random object is instanced without a seed, the seed will be the same as the system time in milliseconds.
        This ensures that, unless two Random objects are instanced in the same millisecond, they will produce different pseudo-random sequences.
        http://stackoverflow.com/questions/5533191/java-random-always-returns-the-same-number-when-i-set-the-seed
        */

        System.out.println("for loop:");
        for(i=0; i< myArray.length;i++)
        {
            //nextInt(int n): pseudorandom, uniformily distributed int value between 0 (inclusive), and specified value (exclusive)
            //example: to generate numbers from min to max (including both):
            //int x = r.nextInt(max - min + 1) + min
            //int x = r.nextInt(10) + 1; //print pseudorandom number between 1 and 10, inclusive
            System.out.println(r.nextInt());
            //System.out.println(r.nextInt(10)+1); //print pseudorandom number between 1 and 10, inclusive
        }
        {
            System.out.println("\nEnhanced for loop:");
            for(int n: myArray)
            {
                System.out.println(r.nextInt());
            }

            System.out.println("\nwhile loop:");
            i=0; //reassign to 0
            while (i < myArray.length)
            {
                System.out.println(r.nextInt());
                i++;
            }
            
            i=0; //reassign to 0
            System.out.println("\ndo...while loop:");
            do
                {
                    System.out.println(r.nextInt());
                    i++;
                }
            while (i < myArray.length);
        }
    }
}