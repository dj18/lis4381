#A1 LIS4381 - Mobile Web Applications Development

## Darion Jattansingh

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio
    - Provide Screenshot of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide GIT command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Developed "Healthy Recipes" Application
    - Edited strings.xml file
    - Edited colors.xml file

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Created an ERD in MySQL and inserted values
    - Forward-Engineered Database
    - Created Android App that uses XML and Java

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Develop and design an interactive business card
    - Include contact details and photo
    - Add border to photo and details button

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create index.php file
    - Add jQuery validation expressions per entity attribute requirements
    - Carousel with responsive images using boostrap

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server-side validation
    - Debugging Processes
    - Testing

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - 