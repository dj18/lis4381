> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381-0001

## Darion Jattansingh

### A5 # Requirements:

*Sub-Heading:*

1. Create a web apllication that uses server-side and client-side validation
2. Use jQuery, PHP, Javascript, HTML, and CSS
3. Questions

#### README.md file should include the following items:

* Screenshot of Error;
* Screenshot of table;

#### Assignment Screenshots:

*Screenshot of Error:

![Error Screenshot](img/screenshot1.png)

*Screenshot of table:

![Table Screenshot](img/screenshot2.png)

