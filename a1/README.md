> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381-0001

## Darion Jattansingh

### A1 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions

#### README.md file should include the following items:

* Screenshot of hwapp application running
* Screenshot of aspnetcoreapp application running My .NET Core Installation;
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initializes a new Git repository
2. git status - displays the state of the current directory
3. git add - saves change from the current directory to staging area
4. git commit - snapshot of projects currently saved changes
5. git push - upload content from a local repository to a remote such as bitbucket
6. git pull - retrieve and store content from the remote repository to update the local repository
7. git reset - resets staging area to clone last commit

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

#### BitbucketStationLocations URL: 


#### Tutorial Links:

*BitbucketStationLocations URL: 
[A1 Bitbucket Station Locations Tutorial Link] (https://bitbucket.org/dj18/bitbucketstationlocations/) "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")