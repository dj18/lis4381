> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381-0001

## Darion Jattansingh

### A3 # Requirements:

*Sub-Heading:*

1. Create an ERD
2. Create an application for purchasing concert tickets
3. Questions

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;
* Links to the following files:
*   a.a3.mwb
*   b.a3.sql

#### Assignment Screenshots:

*Screenshot of ERD:

![ERD Screenshot](img/erd.png)

*Screenshot of running application's first user interface:

![Concert First Activity Screenshot](img/first.png)

*Screenshot of running application's second user interface:

![Concert Second Activity Screenshot](img/second.png)

#### Assignment Links: 

1. [a3 mwb file](docs/a3.mwb)
2. [a3 sql](docs/a3.sql)